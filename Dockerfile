FROM ubuntu:bionic
LABEL maintainer "Jacques Supcik <jacques.supcik@hefr.ch>"

RUN apt-get update && apt-get install -y \
    clang \
    clang-tools \
    cmake \
    curl \
    file \
    git \
    make \
    python3 \
    python3-pip \
    unzip

ENV TOOLCHAIN_DIR="8-2019q3/RC1.1"
ENV TOOLCHAIN_NAME="gcc-arm-none-eabi-8-2019-q3-update"

ENV TOOLCHAIN=/opt/${TOOLCHAIN_NAME}
ENV PATH=${PATH}:${TOOLCHAIN}/bin

RUN cd /opt && curl -sL https://developer.arm.com/-/media/Files/downloads/gnu-rm/${TOOLCHAIN_DIR}/${TOOLCHAIN_NAME}-linux.tar.bz2 | tar jxf -

RUN pip3 install chardet cpplint
